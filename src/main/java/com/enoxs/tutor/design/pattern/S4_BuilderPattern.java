package com.enoxs.tutor.design.pattern;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class S4_BuilderPattern {
    // Step 01
    public interface Item {
        public String name();

        public Packing packing();

        public float price();
    }

    public interface Packing {
        public String pack();
    }

    // Step 02
    public class Wrapper implements Packing {
        @Override
        public String pack() {
            return "Wrapper";
        }
    }

    public class Bottle implements Packing {
        @Override
        public String pack() {
            return "Bottle";
        }
    }

    // Step 03
    public abstract class Burger implements Item {
        @Override
        public Packing packing() {
            return new Wrapper();
        }

        @Override
        public abstract float price();
    }

    public abstract class ColdDrink implements Item {
        @Override
        public Packing packing() {
            return new Bottle();
        }

        @Override
        public abstract float price();
    }

    // Step 04
    public class VegBurger extends Burger {
        @Override
        public float price() {
            return 25.0f;
        }

        @Override
        public String name() {
            return "Veg Burger";
        }
    }

    public class ChickenBurger extends Burger {
        @Override
        public float price() {
            return 50.5f;
        }

        @Override
        public String name() {
            return "Chicken Burger";
        }
    }

    public class Coke extends ColdDrink {
        @Override
        public float price() {
            return 30.0f;
        }

        @Override
        public String name() {
            return "Coke";
        }
    }

    public class Pepsi extends ColdDrink {
        @Override
        public float price() {
            return 35.0f;
        }

        @Override
        public String name() {
            return "Pepsi";
        }
    }

    // Step 05
    public class Meal {
        private List<Item> items = new ArrayList<Item>();

        public void addItem(Item item) {
            items.add(item);
        }

        public float getCost() {
            float cost = 0.0f;
            for (Item item : items) {
                cost += item.price();
            }
            return cost;
        }

        public void showItems() {
            for (Item item : items) {
                System.out.print("Item : " + item.name());
                System.out.print(", Packing : " + item.packing().pack());
                System.out.println(", Price : " + item.price());
            }
        }
    }

    // Step 06
    public class MealBuilder {
        public MealBuilder() {
        }

        public Meal prepareVegMeal() {
            Meal meal = new Meal();
            meal.addItem(new VegBurger());
            meal.addItem(new Coke());
            return meal;
        }

        public Meal prepareNonVegMeal() {
            Meal meal = new Meal();
            meal.addItem(new ChickenBurger());
            meal.addItem(new Pepsi());
            return meal;
        }
    }

    @Test
    public void builderPatternTest() {
        MealBuilder mealBuilder = new MealBuilder();

        Meal vegMeal = mealBuilder.prepareVegMeal();
        System.out.println("Veg Meal");
        vegMeal.showItems();
        System.out.println("Total Cost: " + vegMeal.getCost());

        Meal nonVegMeal = mealBuilder.prepareNonVegMeal();
        System.out.println("\n\nNon-Veg Meal");
        nonVegMeal.showItems();
        System.out.println("Total Cost: " + nonVegMeal.getCost());
    }
}
