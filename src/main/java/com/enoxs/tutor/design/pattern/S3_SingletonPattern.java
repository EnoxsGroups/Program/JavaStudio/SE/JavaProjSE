package com.enoxs.tutor.design.pattern;

import org.junit.Test;

public class S3_SingletonPattern {
    private static S3_SingletonPattern instance = new S3_SingletonPattern();

    //make the constructor private so that this class cannot be
    //instantiated
    private S3_SingletonPattern(){}

    //Get the only object available
    public static S3_SingletonPattern getInstance(){
        return instance;
    }

    public void showMessage(){
        System.out.println("Hello World!");
    }

    public static void main(String[] args) {
        //illegal construct
        //Compile Time Error: The constructor SingleObject() is not visible
        //SingleObject object = new SingleObject();

        //Get the only object available
        S3_SingletonPattern object = S3_SingletonPattern.getInstance();

        //show the message
        object.showMessage();
    }
}
