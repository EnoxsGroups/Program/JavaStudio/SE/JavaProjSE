package com.enoxs.tutor.java8;

import org.junit.Test;

public class S5_DefaultMethod{

    @Test
    public void defaultMethodsTest() {
        AppInfoService appInfoService = new AppInfoServiceImpl();
        appInfoService.info();
    }

    interface AppInfoService {
        default void info() {
            System.out.println("AppInfoService()");
        }

        static void printMsg(String msg) {
            System.out.println(msg);
        }
    }

    interface AppGroupService {
        default void info() {
            System.out.println("AppGroupService()");
        }
    }

    class AppInfoServiceImpl implements AppInfoService, AppGroupService {
        public void info() {
            AppInfoService.super.info();
            AppGroupService.super.info();
            AppInfoService.printMsg("AppInfoServiceImpl -> printMsg()");
            System.out.println("AppInfoServiceImpl()");
        }
    }
}
