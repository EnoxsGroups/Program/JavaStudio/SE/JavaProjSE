package com.enoxs.tutor.java8;

import org.junit.Test;

import java.util.LinkedHashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class S2_LambdaExpression {

    /**
     * Practice example
     */

    interface HistoryService {
        String query(String name,String version);
    }

    @Test
    public void example01(){
        Map<String,String> commitVersion = new LinkedHashMap<String,String>(){{
            put("1.0.1","fix: A");
            put("1.0.2","add: C");
            put("1.0.3","rm: A");
        }};
        Map<String,Map<String,String>> appName = new LinkedHashMap<String,Map<String,String>>(){{
            put("JavaProjSE",commitVersion);
        }};

        HistoryService historyService = (String name, String version) -> appName.get(name).get(version);

        String version = "1.0.1";

        String actual = historyService.query("JavaProjSE",version);
        String expect = "fix: A";// commitVersion.get(version);
        assertEquals(expect,actual);
    }


    /**
     * tutorials point example
     */

    interface MathOperation {
        int operation(int a, int b);
    }

    interface GreetingService {
        void sayMessage(String message);
    }

    @Test
    public void lambdaTest() {
        //with type declaration
        MathOperation addition = (int a, int b) -> a + b;

        //with out type declaration
        MathOperation subtraction = (a, b) -> a - b;

        //with return statement along with curly braces
        MathOperation multiplication = (int a, int b) -> { return a * b; };

        //without return statement and without curly braces
        MathOperation division = (int a, int b) -> a / b;

        System.out.println("10 + 5 = " + operate(10, 5, addition));
        System.out.println("10 - 5 = " + operate(10, 5, subtraction));
        System.out.println("10 x 5 = " + operate(10, 5, multiplication));
        System.out.println("10 / 5 = " + operate(10, 5, division));

        //without parenthesis
        GreetingService greetService1 = message ->
                System.out.println("Hello " + message);

        //with parenthesis
        GreetingService greetService2 = (message) ->
                System.out.println("Hello " + message);

        greetService1.sayMessage("Enoxs");
        greetService2.sayMessage("RHZ");
    }

    private int operate(int a, int b, MathOperation mathOperation) {
        return mathOperation.operation(a, b);
    }
}
