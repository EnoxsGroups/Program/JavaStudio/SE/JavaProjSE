package com.enoxs.tutor.java8;

import org.junit.Test;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;

public class S8_NashornJavaScript {
    @Test
    public void nashornJavaScriptTest(){
        ScriptEngineManager scriptEngineManager = new ScriptEngineManager();
        ScriptEngine engin = scriptEngineManager.getEngineByName("nashorn");

        String name = "Enoxs";
        Integer result = null;

        try {
            engin.eval("print('" + name + "')");

            result = (Integer) engin.eval("10 + 2");

        } catch(ScriptException e) {
            System.out.println("Error executing script: "+ e.getMessage());
        }
        System.out.println(result.toString());
    }
    /**
     * JavaScript Sample Test
     *
     * $ cd /src/test/java/com/enoxs/tutor/java8
     * $ jjs s8-javascript-sample.js
     *
     */
}
