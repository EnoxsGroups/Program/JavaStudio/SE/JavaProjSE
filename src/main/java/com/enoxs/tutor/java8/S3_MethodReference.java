package com.enoxs.tutor.java8;

import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

public class S3_MethodReference {
    /**
    * tutorials point example
    */
    @Test
    public void methodReferenceTest() {
        List numbers = new ArrayList();

        numbers.add("001");
        numbers.add("002");
        numbers.add("003");
        numbers.add("004");
        numbers.add("005");

        numbers.forEach(System.out::println);
    }
}
