package com.enoxs.tutor.java8;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class S1_Java8Overview {

    @Test
    public void java8Test(){
        List<String> names1 = new ArrayList<String>();
        names1.add("001");
        names1.add("003");
        names1.add("005");
        names1.add("004");
        names1.add("002");

        List<String> names2 = new ArrayList<String>();
        names2.add("001");
        names2.add("003");
        names2.add("005");
        names2.add("004");
        names2.add("002");

        System.out.println("Sort using Java 7 syntax: ");

        sortUsingJava7(names1);
        System.out.println(names1);
        System.out.println("Sort using Java 8 syntax: ");

        sortUsingJava8(names2);
        System.out.println(names2);

    }


    //sort using java 7
    private void sortUsingJava7(List<String> names) {
        Collections.sort(names, new Comparator<String>() {
            @Override
            public int compare(String s1, String s2) {
                return s1.compareTo(s2);
            }
        });
    }

    //sort using java 8
    private void sortUsingJava8(List<String> names) {
        Collections.sort(names, (s1, s2) -> s1.compareTo(s2));
    }
}
