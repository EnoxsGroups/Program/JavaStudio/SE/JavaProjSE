package com.enoxs.tutor.java8;

import org.junit.Test;

import java.util.*;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static junit.framework.TestCase.assertEquals;
import static org.junit.Assert.assertTrue;

public class S6_Stream {


    class DataInfo {
        private Long id;
        private String name;
        private String type;

        public Long getId() {
            return id;
        }

        public void setId(Long id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public String toString() {
            return "DataInfo{" +
                    "id=" + id +
                    ", name='" + name + '\'' +
                    ", type='" + type + '\'' +
                    '}';
        }
    }


    /**
     * Practice example
     */
    @Test
    public void example01() {
        String expect, actual;

        List<DataInfo> lstDataInfo = new LinkedList<DataInfo>() {{
            DataInfo info01 = new DataInfo();
            info01.setId(Long.valueOf(1));
            info01.setName("001");
            info01.setType("A");
            DataInfo info02 = new DataInfo();
            info02.setId(Long.valueOf(2));
            info02.setName("002");
            info02.setType("A");
            DataInfo info03 = new DataInfo();
            info03.setId(Long.valueOf(3));
            info03.setName("003");
            info03.setType("B");

            add(info01);
            add(info02);
            add(info03);
        }};


        // Filter
        Long count = lstDataInfo.stream().filter(dataInfo -> "A".equals(dataInfo.getType())).count();
        actual = count + "";
        expect = "2";
        assertEquals(expect, actual);

        List<DataInfo> lstFilter = lstDataInfo.stream().filter(dataInfo -> "A".equals(dataInfo.getType())).collect(Collectors.toList());
        lstFilter.forEach(System.out::println);

        List<String> lstMsg = Arrays.asList("001", "002", "003", "A", "A", "B", "B", "C");

        String joinMsg = lstMsg.stream().filter(msg -> msg.equals("A")).collect(Collectors.joining(", "));

        actual = joinMsg;
        expect = "A, A";
        assertEquals(expect, actual);
        System.out.printf("joinMsg = %s%n", joinMsg);

        List<Integer> lstNumber = Arrays.asList(1, 3, 3, 3, 5);
        List<Integer> lstResult = lstNumber.stream().map(num -> num * 3).distinct().collect(Collectors.toList());
        lstResult.forEach(System.out::println);

        assertTrue(lstResult.size() == 3);
        actual = lstResult.get(0) + "";
        expect = "3";
        assertEquals(expect, actual);

        actual = lstResult.get(1) + "";
        expect = "9";
        assertEquals(expect, actual);

        actual = lstResult.get(2) + "";
        expect = "15";
        assertEquals(expect, actual);

        IntSummaryStatistics stats = lstNumber.stream().mapToInt(num -> num).summaryStatistics();

        System.out.printf("Max = %s%n", stats.getMax());
        System.out.printf("Min = %s%n", stats.getMin());
        System.out.printf("Sum = %s%n", stats.getSum());
        System.out.printf("Average = %s%n", stats.getAverage());
        System.out.printf("Count = %s%n", stats.getCount());

        actual = stats.getMax()+"";
        expect = "5";
        assertEquals(expect,actual);
        actual = stats.getMin()+"";
        expect = "1";
        assertEquals(expect,actual);
        actual = stats.getSum()+"";
        expect = "15";
        assertEquals(expect,actual);
        actual = stats.getAverage()+"";
        expect = "3.0";
        assertEquals(expect,actual);
        actual = stats.getCount()+"";
        expect = "5";
        assertEquals(expect,actual);

        List<Integer> lstSeq = Arrays.asList(1, 5, 9, 7, 3);
        List<Integer> lstSort = lstSeq.stream().sorted().collect(Collectors.toList());
        actual = "1";
        expect = lstSort.get(0) + "";
        assertEquals(expect,actual);
        actual = "3";
        expect = lstSort.get(1) + "";
        assertEquals(expect,actual);
        actual = "5";
        expect = lstSort.get(2) + "";
        assertEquals(expect,actual);
        actual = "7";
        expect = lstSort.get(3) + "";
        assertEquals(expect,actual);
        actual = "9";
        expect = lstSort.get(4) + "";
        assertEquals(expect,actual);

        //parallel processing
        count = lstMsg.parallelStream().filter(string -> string.isEmpty()).count();
        System.out.println("Empty Strings: " + count);
        actual = count + "";
        expect = "0";
        assertEquals(expect,actual);
    }


    /**
     * tutorials point example
     */

    List<String> strings = Arrays.asList("001", "002", "003", "", "A", "", "B");

    List<Integer> numbers = Arrays.asList(3, 2, 2, 3, 7, 3, 5);

    List<Integer> integers = Arrays.asList(1, 2, 13, 4, 15, 6, 17, 8, 19);

    @Test
    public void java7StreamTest() {
        System.out.println("Using Java 7: \n");

        System.out.println(" ------ string list ------ \n ");

        // Count empty strings
        System.out.println("List: " + strings);
        long count = getCountEmptyStringUsingJava7(strings);

        System.out.println("Empty Strings: " + count);
        count = getCountLength3UsingJava7(strings);

        System.out.println("Strings of length 3: " + count);

        //Eliminate empty string
        List<String> filtered = deleteEmptyStringsUsingJava7(strings);
        System.out.println("Filtered List: " + filtered);

        //Eliminate empty string and join using comma.
        String mergedString = getMergedStringUsingJava7(strings, ", ");
        System.out.println("Merged String: " + mergedString);

        System.out.println("\n ------ numbers list ------ \n ");

        //get list of square of distinct numbers
        List<Integer> squaresList = getSquares(numbers);
        System.out.println("Squares List: " + squaresList);

        System.out.println("\n ------ integers list ------ \n ");

        System.out.println("List: " + integers);
        System.out.println("Highest number in List : " + getMax(integers));
        System.out.println("Lowest number in List : " + getMin(integers));
        System.out.println("Sum of all numbers : " + getSum(integers));
        System.out.println("Average of all numbers : " + getAverage(integers));
        System.out.println("Random Numbers: ");

        //print ten random numbers
        Random random = new Random();

        for (int i = 0; i < 10; i++) {
            System.out.println(random.nextInt());
        }

    }

    @Test
    public void java8StreamTest() {
        System.out.println("Using Java 8: \n");
        System.out.println(" ------ integers list ------ \n");
        System.out.println("List: " + strings);

        Long count = strings.stream().filter(string -> string.isEmpty()).count();
        System.out.println("Empty Strings: " + count);

        count = strings.stream().filter(string -> string.length() == 3).count();
        System.out.println("Strings of length 3: " + count);

        List<String> filtered = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.toList());
        System.out.println("Filtered List: " + filtered);

        String mergedString = strings.stream().filter(string -> !string.isEmpty()).collect(Collectors.joining(", "));
        System.out.println("Merged String: " + mergedString);

        System.out.println("\n ------ numbers list ------ \n");

        List<Integer> squaresList = numbers.stream().map(i -> i * i).distinct().collect(Collectors.toList());
        System.out.println("Squares List: " + squaresList);


        System.out.println("\n ------ integers list ------ \n");
        System.out.println("List: " + integers);

        IntSummaryStatistics stats = integers.stream().mapToInt((x) -> x).summaryStatistics();

        System.out.println("Highest number in List : " + stats.getMax());
        System.out.println("Lowest number in List : " + stats.getMin());
        System.out.println("Sum of all numbers : " + stats.getSum());
        System.out.println("Average of all numbers : " + stats.getAverage());
        System.out.println("Random Numbers: ");

        Random random = new Random();
        random.ints().limit(10).sorted().forEach(System.out::println);

        //parallel processing
        count = strings.parallelStream().filter(string -> string.isEmpty()).count();
        System.out.println("Empty Strings: " + count);

    }

    private int getCountEmptyStringUsingJava7(List<String> strings) {
        int count = 0;

        for (String string : strings) {

            if (string.isEmpty()) {
                count++;
            }
        }
        return count;
    }

    private int getCountLength3UsingJava7(List<String> strings) {
        int count = 0;

        for (String string : strings) {

            if (string.length() == 3) {
                count++;
            }
        }
        return count;
    }

    private List<String> deleteEmptyStringsUsingJava7(List<String> strings) {
        List<String> filteredList = new ArrayList<String>();

        for (String string : strings) {

            if (!string.isEmpty()) {
                filteredList.add(string);
            }
        }
        return filteredList;
    }

    private String getMergedStringUsingJava7(List<String> strings, String separator) {
        StringBuilder stringBuilder = new StringBuilder();

        for (String string : strings) {

            if (!string.isEmpty()) {
                stringBuilder.append(string);
                stringBuilder.append(separator);
            }
        }
        String mergedString = stringBuilder.toString();
        return mergedString.substring(0, mergedString.length() - 2);
    }

    private List<Integer> getSquares(List<Integer> numbers) {
        List<Integer> squaresList = new ArrayList<Integer>();

        for (Integer number : numbers) {
            Integer square = new Integer(number.intValue() * number.intValue());

            if (!squaresList.contains(square)) {
                squaresList.add(square);
            }
        }
        return squaresList;
    }

    private int getMax(List<Integer> numbers) {
        int max = numbers.get(0);

        for (int i = 1; i < numbers.size(); i++) {

            Integer number = numbers.get(i);

            if (number.intValue() > max) {
                max = number.intValue();
            }
        }
        return max;
    }

    private int getMin(List<Integer> numbers) {
        int min = numbers.get(0);

        for (int i = 1; i < numbers.size(); i++) {
            Integer number = numbers.get(i);

            if (number.intValue() < min) {
                min = number.intValue();
            }
        }
        return min;
    }

    private int getSum(List numbers) {
        int sum = (int) (numbers.get(0));

        for (int i = 1; i < numbers.size(); i++) {
            sum += (int) numbers.get(i);
        }
        return sum;
    }

    private int getAverage(List<Integer> numbers) {
        return getSum(numbers) / numbers.size();
    }
}
