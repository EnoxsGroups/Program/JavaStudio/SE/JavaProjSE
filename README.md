Java Project Simple Example - Version SE1
======
+ name: `JavaProjSE`
+ version: `SE1` 
+ date: `2020-02-25`
+ author: `Enoxs`
+ remark: `Java Project Simple Example - Version SE1`

Contents
------
+ tutor
    + java
    + java8
    + design pattern

Reference
------
+ [JavaProjSE-v1.0.2](https://github.com/RHZEnoxs/JavaProjSE-v1.0.2)
+ [JavaProjSE-v1.0.3](https://gitlab.com/EnoxsGroups/Program/JavaStudio/SE/JavaProjSE-v1.0.3.git)